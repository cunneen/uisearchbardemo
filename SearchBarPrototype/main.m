//
//  main.m
//  SearchBarPrototype
//
//  Created by Mike Cunneen 10/12/2012.
//  Copyright (c) 2012 Mike Cunneen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
