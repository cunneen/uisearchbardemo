UISearchBarDemo
===============

UISearchBarDemo is a simple working demonstration of how to use a UISearchBar (search bar) in an iOS application, from Objective-C.  It illustrates an autocomplete scenario.

Screenshots
----
![screenshot](http://i.imgur.com/pcWap.png)



How To Use It
-------------

### Requirements
Requires a deployment target >= iOS 5.0.

### Usage
Open the SearchBarPrototypeViewController.  The search bar in this example is backed by 
a mutable array called dummySearchData.

With every keystroke, the shouldReloadTableForSearchString method is invoked.  It clears the dummySearchData array completely, and in turn invokes the handleSearchForSearchString method.  You would typically remove the NSLog statement.

The handleSearchForSearchString just adds ten items, all of which contain the search string.  You would typically customise this method too add items which meet your own criteria, based on the search string.

If the user selects one of the suggestions, the didSelectRowAtIndexPath method is invoked.  At the moment this just displays an alert.  You would typically have this perform some other custom action.

Finally, you would typically remove the webView and simply delete all lines of code which contain the word "webView".

Look for the comments marked "TODO".


