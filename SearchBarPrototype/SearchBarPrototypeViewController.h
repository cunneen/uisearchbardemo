//
//  SearchBarPrototypeViewController.h
//  SearchBarPrototype
//
//  Created by Mike Cunneen 10/12/2012.
//  Copyright (c) 2012 Mike Cunneen. All rights reserved.
//

@interface SearchBarPrototypeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate> {
    NSArray *searchResultPlaces;
    
    BOOL shouldBeginEditing;
}


@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) NSMutableArray* dummySearchData;
@end
