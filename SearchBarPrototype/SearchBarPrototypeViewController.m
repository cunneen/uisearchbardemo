//
//  SearchBarPrototypeViewController.h
//  SearchBarPrototype
//
//  Created by Mike Cunneen 10/12/2012.
//  Copyright (c) 2012 Mike Cunneen. All rights reserved.
//

#import "SearchBarPrototypeViewController.h"

@interface SearchBarPrototypeViewController ()

@end

@implementation SearchBarPrototypeViewController
@synthesize webView;
@synthesize dummySearchData;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       shouldBeginEditing = YES;
    }
    return self;
}

- (void)viewDidLoad {
    self.searchDisplayController.searchBar.placeholder = @"Search or Address";
    dummySearchData =  [[NSMutableArray alloc] initWithCapacity:10];
    //TODO: remove these three lines; they load an HTML doc in the webView
    NSString* htmlPath = [[NSBundle mainBundle] pathForResource:@"dummyPage" ofType:@"html"];
    NSString* appHtml = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
    [webView loadHTMLString:appHtml baseURL:nil];
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [super viewDidUnload];
}

- (void)dealloc {
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"SearchBarPrototypeAutocompleteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@%@", [dummySearchData objectAtIndex:indexPath.item],@" extraText"];
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate



- (void)dismissSearchControllerWhileStayingActive {
    // Animate out the table view.
    NSTimeInterval animationDuration = 0.3;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.searchDisplayController.searchResultsTableView.alpha = 0.0;
    [UIView commitAnimations];
    
    [self.searchDisplayController.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchDisplayController.searchBar resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //TODO: actually do something with a selection.  This just alerts.
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selection" message:[NSString stringWithFormat:@"row %d selected, value: %@", indexPath.item, [dummySearchData objectAtIndex:indexPath.item]] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alert show];
    
}

#pragma mark -
#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString {
    NSLog(@"Searched for string: %@", searchString);
    // add search items for this search string
    //TODO: This is where we add items that match our custom query.
    for(int i=0; i<10; i++) { // this example just adds 10 items that include our query.
        [dummySearchData insertObject:[NSString stringWithFormat:@"item matching %@ # %d", searchString, i] atIndex:i];
    }
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    NSLog(@"searchDisplayControllershouldReloadTableForSearchString");
    // clear out the search results.
    [dummySearchData removeAllObjects];
    // add new search results
    [self handleSearchForSearchString:searchString];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

#pragma mark -
#pragma mark UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (![searchBar isFirstResponder]) {
        // User tapped the 'clear' button.
        shouldBeginEditing = NO;
        [self.searchDisplayController setActive:NO];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (shouldBeginEditing) {
        // Animate in the table view.
        NSTimeInterval animationDuration = 0.3;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        self.searchDisplayController.searchResultsTableView.alpha = 1.0;
        [UIView commitAnimations];
        
        [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:YES];
    }
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}


@end
