//
//  AppDelegate.h
//  SearchBarPrototype
//
//  Created by Mike Cunneen 10/12/2012.
//  Copyright (c) 2012 Mike Cunneen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
