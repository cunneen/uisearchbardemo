//
//  AppDelegate.m
//  SearchBarPrototype
//
//  Created by Mike Cunneen 10/12/2012.
//  Copyright (c) 2012 Mike Cunneen. All rights reserved.
//

#import "SearchBarPrototypeViewController.h"
#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];    
    
    SearchBarPrototypeViewController *viewController = [[SearchBarPrototypeViewController alloc] init];
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)dealloc {
}

@end
